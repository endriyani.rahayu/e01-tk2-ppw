"""PPWGroupProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from support import views as support
from feedback import views as feedback_views
from kuesioner import views as kuesioner
from articles import views as articles
from authen import views as authen
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('duar.urls')),
    path('formulirsupport/', support.formulir_support, name = 'formulirsupport'),
    path('landingsupport/', support.landingsupport, name = 'landingsupport'),
    path('support/', support.support, name = 'support'),
    path('AjaxSupportDetail/', support.ajax_support, name = 'ajaxsupport'),
    path('supportJSONData/', support.manage_JSON, name='supportJSONData'),

    path('articles/', articles.articles_page, name = 'articles'),
    path('insertarticles/', articles.insert_articles, name = 'insertarticles'),
    path('insertAjaxArticles/', articles.ajax_articles, name = 'ajax_articles'),
    path('articles/<str:pk>', articles.selengkapnya, name = 'selengkapnya'),
    path('deletearticle/<str:pk>', articles.hapus_artikel, name = 'hapus_artikel'),
    path('articlesAPI/', articles.json_req, name='artikel_json'),

    path('feedback/', feedback_views.feedback, name = 'feedback'),

    path('login/', authen.login_page, name='login_page'),
    path('register/', authen.register_page, name='register_page'),
    path('logout/', authen.logout_page, name='logout_page'),

    path('kuesioner/', kuesioner.formulir_kuesioner, name = 'kuesioner'),
    path('listkuesioner/', kuesioner.list_kuesioner, name = 'listkuesioner'),
    path('hasilkuesioner/<str:pk>/', kuesioner.hasil_kuesioner, name = 'hasilkuesioner'),
    path('jsonkuesioner/', kuesioner.query_kuesioner, name='query_kuesioner')
]

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)


