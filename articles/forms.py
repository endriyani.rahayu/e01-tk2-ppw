from django import forms
from django.forms import ModelForm, Textarea, TextInput, DateInput
from .models import Articles
from crispy_forms.helper import FormHelper
from django.contrib.admin.widgets import AdminDateWidget


KATEGORI_CHOICES = (
    ('Umum', 'Umum'),
    ('Kesehatan', 'Kesehatan'),
    ('Teknologi', 'Teknologi'),
    ('Politik', 'Politik'),
    ('Ekonomi', 'Ekonomi'),
    ('Sosial', 'Sosial'),
    ('Pendidikan', 'Pendidikan'),
    ('Makanan', 'Makanan'),
    ('Alam', 'Alam'),
)

class ArticlesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ArticlesForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        # self.fields['penulis'].label = 'Penulis'
        self.fields['kategori'].label = 'Kategori'
        self.fields['tanggal'].label = 'Tanggal'
        self.fields['judul'].label = 'Judul'
        self.fields['isi_artikel'] = forms.CharField(widget=forms.Textarea, min_length=300, max_length=1000)
        self.fields['isi_artikel'].label = 'Isi Artikel'
        self.fields['gambar_artikel'].label = 'Gambar Artikel'

    class Meta:
        model = Articles
        fields = ['penulis', 'kategori', 'tanggal', 'judul', 'isi_artikel', 'gambar_artikel']
        widgets = {'penulis': forms.HiddenInput()}