from django.shortcuts import render, redirect
from .models import Articles
from .forms import ArticlesForm
from django.http import HttpResponse
from django.http import JsonResponse
from django.core import serializers
from django.http import HttpResponse
from django.contrib.auth.models import Permission, User
import json
from django.core.serializers.json import DjangoJSONEncoder


from django.contrib.auth.decorators import login_required


# Create your views here.
def articles_page(request):
    semua_artikel = Articles.objects.all()
    jumlah_semua_artikel = semua_artikel.count()
    context = {'semua_artikel': semua_artikel, 'jumlah_semua_artikel': jumlah_semua_artikel}
    return render(request, 'articles.html', context)

@login_required(login_url='/login')
def insert_articles(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ArticlesForm(request.POST, request.FILES)
        # check whether it's valid:
        if form.is_valid():
            form.save()
            return redirect('articles')
        # if a GET (or any other method) we'll create a blank form
    else:
        form = ArticlesForm(initial={ 'penulis' : request.user.username})
    return render(request, 'insertarticles.html', {'form': form})

def selengkapnya(request, pk):
    satu_artikel = Articles.objects.get(id = pk)
    return render(request, 'selengkapnya.html', {'satu_artikel': satu_artikel})

@login_required(login_url='/login')
def hapus_artikel(request, pk):
    satu_artikel = Articles.objects.get(id=pk)
    if request.user.username == satu_artikel.penulis:
        if request.method == "POST":
            satu_artikel.delete()
            return redirect("articles")
    else:
        return HttpResponse("Forbidden Query")

    context = {'satu_artikel': satu_artikel}
    return render(request, 'konfirmhapus.html', context)

@login_required(login_url='/login')
def ajax_articles(request):
    if request.method == "POST":
        kategori = request.POST['kategori']
        tanggal = request.POST['tanggal']
        judul = request.POST['judul']
        isi_artikel = request.POST['isi_artikel']
        gambar_artikel = request.FILES['gambar_artikel']
        Articles.objects.create(
            penulis = request.user.username,
            kategori = kategori,
            tanggal = tanggal,
            judul = judul,
            isi_artikel = isi_artikel,
            gambar_artikel = gambar_artikel,
        )

        return HttpResponse('')
    return render(request, 'insertarticlesAjax.html')

def json_req(request):
    serialized_articles = []
    all_articles = Articles.objects.all()
    username = request.user.username
    for article in all_articles:
      serialized_articles.append(article.seralize())
    serialized_articles.append({'username': username})
    return HttpResponse(json.dumps(serialized_articles, sort_keys=True, indent = 1, cls = DjangoJSONEncoder), content_type='application/json')

