from django.test import TestCase
from django.contrib.auth.models import User

# Create your tests here.
LOG_IN_URL = '/login/'
SIGN_UP_URL = '/register/'
LOG_OUT_URL = '/logout/'

class UnitTest(TestCase):
    # Test untuk mengetahui apakah URL telah tersedia
    def setUp(self):
        user = User.objects.create_user(username='john', password='johnpassword')
        user.save()

    def test_url_login_is_exist(self):
        response = self.client.get(LOG_IN_URL)
        self.assertEqual(response.status_code, 200)

    def test_url_register_is_exist(self):
        response = self.client.get(SIGN_UP_URL)
        self.assertEqual(response.status_code, 200)

    def test_using_template(self):
        response = self.client.get(LOG_IN_URL)
        self.assertTemplateUsed(response, "login.html")

    def test_register_using_template(self):
        response = self.client.get(SIGN_UP_URL)
        self.assertTemplateUsed(response, "register.html")

    def test_create_new_user_from_url(self):
        total_user = User.objects.all().count()
        passed_data = {
            'username': 'thisisusername',
            'first_name' : 'Ariasena',
            'password1': 'ThisIsAP@ssword123',
            'password2': 'ThisIsAP@ssword123',
        }
        response = self.client.post(SIGN_UP_URL, data=passed_data)
        self.assertEqual(response.status_code, 302)
        self.assertEquals(User.objects.all().count(), total_user + 1)

    def test_success_signin_john(self):
        passed_data = {
            'username': 'john',
            'password': 'johnpassword'
        }
        response = self.client.post(LOG_IN_URL, data=passed_data)
        self.assertEquals(response.status_code, 302)

    def test_failed_signin_john(self):
        passed_data = {
            'username': 'john',
            'password': 'johnpassword1'
        }
        response = self.client.post(LOG_IN_URL, data=passed_data)
        self.assertEquals(response.status_code, 200)
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(str(messages[0]), 'Username OR password is incorrect')


    def test_logout_user(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get(LOG_OUT_URL)
        self.assertEquals(response.status_code, 302)