from django import forms
from .models import Feedback
from mptt.forms import TreeNodeChoiceField

class FeedbackForm(forms.ModelForm):
	parent = TreeNodeChoiceField(queryset=Feedback.objects.all())
	def __init__(self, *args, **kwargs):
		super().__init__(*args,**kwargs)
		self.fields['parent'].widget.attrs.update({'class':'d-none'})
		self.fields['parent'].label=''
		self.fields['parent'].required = False

	class Meta:
		model = Feedback
		fields = ('user','parent','body','score')
		widgets = {
				'user':forms.TextInput(attrs={'class':'form-control','placeholder':'Your name'}),
				'body':forms.Textarea(attrs={'class':'form-control', 'placeholder':'Type Your Feedback'})
		}
	def customSave(self, user):
		data = self.save(commit=False)
		data.save()
		return data