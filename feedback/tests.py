from django.test import TestCase, Client
from .models import Feedback
from django.apps import apps
from .apps import FeedbackConfig

# Create your tests here.

class FeedbackConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(FeedbackConfig.name, 'feedback')
        self.assertEqual(apps.get_app_config('feedback').name, 'feedback')

class FeedbackTestCase(TestCase):
    def test_url_status_200(self):
        response = Client().get('/feedback/')
        self.assertEqual(200, response.status_code)

    def test_template_feedback(self):
       response = Client().get('/feedback/')
       self.assertTemplateUsed(response, 'feedback.html')
    
    def test_view_feedback(self):
        response = Client().get('/feedback/')
        html_response = response.content.decode ('utf8')
        self.assertIn("Help Us Improve", html_response)
        self.assertIn("Post Your Feedback", html_response)
        self.assertIn("Silakan login untuk menambahkan feedback", html_response)
    

    def test_form_feedback(self):
        Client().post('/feedback/', data={'user' : 'Captain', 'body' : 'Not Bad','score':0})
        amount = Feedback.objects.count()
        self.assertEqual(amount, 1)

    def test_form_feedback(self):
        Client().post('/feedback/', data={'user' : 'Captain', 'body' : 'Not Bad','score':2})
        amount = Feedback.objects.count()
        self.assertEqual(amount, 1)


    def test_string_user_name(self):
        object_feedback = Feedback(user="Hakim")
        self.assertEqual(str(object_feedback), "Feedback by "+object_feedback.user)