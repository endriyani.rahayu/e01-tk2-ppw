from django.db import models

# Create your models here.
class Kuesioner(models.Model):
    Nama = models.CharField(max_length = 100)
    Pertanyaan1 = models.BooleanField(default=False)
    Pertanyaan2 = models.BooleanField(default=False)
    Pertanyaan3 = models.BooleanField(default=False)
    Pertanyaan4 = models.BooleanField(default=False)
    Pertanyaan5 = models.BooleanField(default=False)
    Pertanyaan6 = models.BooleanField(default=False)

    def __str__(self):
        return self.Nama


    @property
    def hitung_nilai(self):
        nilai = 0
        if self.Pertanyaan1:
            nilai += 1
        if self.Pertanyaan2:
            nilai += 1
        if self.Pertanyaan3:
            nilai += 1
        if self.Pertanyaan4:
            nilai += 1
        if self.Pertanyaan5:
            nilai += 5
        if self.Pertanyaan6:
            nilai += 5
        return nilai
    
    Nilai = 0