from django.shortcuts import render, redirect
from .forms import SupportForm
from django.contrib.auth.decorators import login_required

from django.core.serializers import serialize
from django.http import HttpResponse
import json
from django.core.serializers.json import DjangoJSONEncoder
from .models import Support


# Create your views here.
@login_required(login_url='/login')
def formulir_support(request):
    form = SupportForm()
    if request.method == "POST":
        form = SupportForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/landingsupport')
    response = {'form' : form}
    return render(request, 'formulirsupport.html', response)

@login_required(login_url='/login')
def landingsupport(request):
    response = {}
    return render(request, 'landingsupport.html', response)


def support(request):
    response = {}
    return render(request, 'support.html', response)

def manage_JSON(request):
    serializer = []
    supports = Support.objects.all()
    username = request.user.username
    serializer.append({'username': username})
    
    for request in supports:
        ser = request.serialize()
        serializer.append(ser)
    return HttpResponse(json.dumps(serializer, indent = 1, cls = DjangoJSONEncoder), content_type='application/json')

def ajax_support(request):
    response ={}
    return render(request, 'ajax_support.html', response)

def call_AJAX(request):

    url_tujuan = "https://duartkpepew.herokuapp.com/supportJSONData/"
    req = request.get(url_tujuan)
    data = req.json()
    return JsonResponse(data, safe=False)
